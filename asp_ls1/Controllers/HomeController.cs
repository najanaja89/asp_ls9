﻿using asp_ls1.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace asp_ls1.Controllers
{
    public class HomeController : Controller
    {
        DataContext context = new DataContext();
        //User user = new User { Name = "Admin" };

        List<Product> products = new List<Product>
        {
            new Product { Id = 1, Name="Cheese", Price= 1200},
            new Product { Id = 2, Name="Milk", Price= 600},
            new Product { Id = 3, Name="Meat", Price= 1500},
            new Product { Id = 4, Name="Blood", Price= 21500},
            new Product { Id = 5, Name="Soul", Price= 6500}
        };

        private IShopService service;

        public HomeController()
        {
            //service = fabric.GetService<IShopService>;
            service = new ShopService(new ProductRepository());
        }
        public ActionResult Index()
        {
            var agent = HttpContext.Request.Headers["user-agent"];
            //HttpContext.Response.ContentType="text/plain";
            HttpContext.Response.ContentType = "text/json";
            var product = new Product { Name = "json", Price = 1200 };
            var json = JsonConvert.SerializeObject(product);

            return Content(json);

            var cookie = new HttpCookie("username", "user"); //название куки, значение
            cookie.Expires = DateTime.Now.AddMinutes(5);
            HttpContext.Response.Cookies.Add(cookie);
            var req = HttpContext.Request.Cookies["username"];//вытаскиваем cookie
            req.Expires = DateTime.Now.AddDays(-1);//Удаляем куки
            Session["username"] = "user";
            Session.Timeout = 20; //Тайм аут сессии, по умолчанию 20 минут, если не отправлять запросы
            Session.Abandon(); //Удаляем сессию
            var s = Session.IsNewSession; //Новая сессия или нет
                                                                                                                                                    

            //return View();
        }

        //Пример куки
        [HttpPost]
        public ActionResult SetLanguage(int id)
        {
            HttpContext.Response.Cookies.Add(new HttpCookie("lang", id.ToString()));
            return new EmptyResult();
        }

        public ViewResult Admin()
        {
            //context.users.Add(user);
            return View(context.users.Where(u => u.Login == "admin"));
        }

        public ViewResult AdminAll()
        {
            //context.users.Add(user);
            return View(context.users);
        }

        public ViewResult Products()
        {
            //var products = context.products.ToList();
            //return View(products);
            return View(service.GetProductsByCategory(1));
        }

        public ViewResult Product(int? id) //Параметры get
        {
            var product = context.products.ToList();
            if (id == null)
            {
                return View(product);

            }
            else
            {
                return View(product.FirstOrDefault(p => p.Id == id));
            }
        }

        public ActionResult UserRegistration()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserRegistration(User user)
        {
            if (!ModelState.IsValid)
            {
                return View(user);
            }

            else
            {
                var u = context.users.FirstOrDefault(us => us.Login == user.Login.ToLower());
                if (u != null)
                {
                    user.Id = u.Id;
                    return View(user);
                }
                else
                {
                    user.Login = user.Login.ToLower();
                    context.users.Add(user);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }

            }
        }

        // /Home/CreateProduct

        [HttpGet]
        public ActionResult CreateProduct()
        {
            return View();
        }

        //action="~/Controllers/" в форме поменять путь
        //[Route("/Home/NewProduct")]
        [HttpPost]
        public ActionResult CreateProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return View(product);
            }

            else
            {
                context.products.Add(product);
                context.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        [HttpPut]
        public ActionResult UpdateProduct(Product product)
        {
            context.products.Add(product);
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        //public async Task<ViewResult> About()
        //{ 
        //    await SaveChngesAync
        //}

        public ViewResult About()
        {
            return View();
        }
    }
}