﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace asp_ls1.Models
{
    public interface IShopService
    {
        IEnumerable<Product> GetProductsByCategory(int id);
    }
}
