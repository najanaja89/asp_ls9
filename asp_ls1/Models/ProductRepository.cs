﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace asp_ls1.Models
{
    public class ProductRepository : IRepository<Product>
    {

        List<Product> products = new List<Product>
        {
            new Product { Id = 1, Name="Cheese", Price= 1200, CategoryId = 1 },
            new Product { Id = 2, Name="Milk", Price= 600,CategoryId = 2 },
            new Product { Id = 3, Name="Meat", Price= 1500,CategoryId = 2 },
            new Product { Id = 4, Name="Blood", Price= 21500,CategoryId = 3 },
            new Product { Id = 5, Name="Soul", Price= 6500,CategoryId = 1 }
        };

        public void Create(Product product)
        {
            products.Add(product);
        }

        public IEnumerable<Product> Get()
        {
            return products;
        }

        public Product GetById(int id)
        {
            return products.Where(u => u.Id == id).FirstOrDefault();
        }
    }
}