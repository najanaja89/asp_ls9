﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace asp_ls1.Models
{
    public class ShopService : IShopService
    {
        private readonly IRepository<Product> repository;
        public ShopService(IRepository<Product> repository)
        {
            this.repository = repository;
        }
        public IEnumerable<Product> GetProductsByCategory(int id)
        {
            return repository.Get().Where(p => p.CategoryId == id);
        }
    }
}